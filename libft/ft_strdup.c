/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 18:14:44 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:31:31 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	size_t	length;
	char	*s2;
	char	*temp;

	length = ft_strlen(s1);
	s2 = (char*)malloc(sizeof(char) * (length + 1));
	if (!s2)
		return (NULL);
	temp = s2;
	while (*s1 && length--)
	{
		*s2 = *s1;
		s2++;
		s1++;
	}
	*s2 = '\0';
	return (temp);
}
