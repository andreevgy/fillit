/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/22 13:24:53 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:28:48 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
**	create_map
**	Принимает размер карты, создает ее и возвращает
*/

char		**create_map(int i)
{
	int		y;
	int		x;
	char	**map;

	y = 0;
	NULL_CHECK((map = (char **)malloc(sizeof(char *) * (i + 1))));
	while (y < i)
	{
		x = 0;
		NULL_CHECK((map[y] = (char *)malloc(sizeof(char) * (i + 1))));
		while (x < i)
			map[y][x++] = '.';
		map[y][x] = '\0';
		y++;
	}
	map[y] = NULL;
	return (map);
}

/*
**	create_min_map
**	Принимает количество деталей и возвращает минимальный размер карты
*/

int			calculate_size(int n)
{
	int		i;

	i = 1;
	while (i * i < n * 4)
		i++;
	return (i);
}

int			copy_map(char ***map, char ***new_map, int size)
{
	int		y;

	y = 0;
	NULL_CHECK((*new_map = malloc(sizeof(char *) * (size + 1))));
	y = 0;
	while (y < size)
	{
		NULL_CHECK(((*new_map)[y] = ft_strdup((*map)[y])));
		y++;
	}
	(*new_map)[y] = NULL;
	return (1);
}
