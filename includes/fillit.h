/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/23 16:38:53 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:23:39 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <string.h>
# include <fcntl.h>
# include <stdlib.h>
# include "../libft/includes/libft.h"

# define NULL_CHECK(val) if (val == NULL) exit(12);

typedef struct				s_tetr
{
	int						width;
	int						height;
	char					**figure;
	char					symbol;
	struct s_tetr			*next;
}							t_tetr;

typedef struct				s_coords
{
	int						x;
	int						y;
}							t_coords;

t_tetr						*read_file(char *file_name);
int							ft_figure_validation(char **lines);
t_coords					*create_coords(int x, int y);
void						create_list(t_tetr **list, char **figure);
char						**create_map(int i);
int							calculate_size(int n);
int							solve_map(char ***map, int size, t_tetr *list);
void						print_map(char **map, int size);
void						clean_map(char **map, int size);
int							copy_and_clean(char ***new_map, int size,
							char ***map, t_coords *coords);
int							delete_last(char **new_map, t_tetr *list,
							t_coords *coords, int size);
int							copy_map(char ***map, char ***new_map, int size);
void						clean_lines(char **lines);
#endif
