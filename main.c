/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 18:22:27 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:26:31 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

/*
**	print_map
**	Временная функция для вывода карты
*/

void		print_map(char **map, int size)
{
	int y;

	y = 0;
	while (y < size)
		ft_putendl(map[y++]);
}

/*
**	count_list
**	Функция для расчета количества деталей
*/

static int	count_list(t_tetr *list)
{
	t_tetr	*iter;
	int		n;

	iter = list;
	n = 0;
	while (iter)
	{
		n++;
		iter = iter->next;
	}
	return (n);
}

int			main(int argc, char **argv)
{
	t_tetr	*list;
	char	**map;
	int		size;

	if (argc == 2)
	{
		list = read_file(argv[1]);
		if (!list)
			ft_putstr("error");
		else
		{
			size = calculate_size(count_list(list));
			map = create_map(size);
			while (!solve_map(&map, size, list))
			{
				clean_map(map, size++);
				map = create_map(size);
			}
			print_map(map, size);
			clean_map(map, size);
		}
	}
	else
		ft_putstr("usage: ./fillit tetrominos_file");
	exit(0);
}
