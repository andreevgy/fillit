# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fmacgyve <fmacgyve@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/21 14:28:08 by fmacgyve          #+#    #+#              #
#    Updated: 2018/12/19 18:10:50 by fmacgyve         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME        = fillit
FLAGS       = -Wall -Wextra -Werror
SRC_DIR		= ./srcs
OBJ_DIR		= ./obj
RAW_CFILES	= main.c read_file.c validation.c create_list.c coords.c map.c solve.c clean_funcs.c
RAW_OFILES	= $(RAW_CFILES:.c=.o)


all: $(NAME)

$(NAME): $(RAW_OFILES)
		make -C ./libft
		gcc $(RAW_OFILES) libft/libft.a -I ./includes -o $(NAME)

%.o: %.c
		gcc -Wall -Wextra -Werror -I ./includes -c $< -o $@
clean:
		make clean -C ./libft
		rm -f $(RAW_OFILES)

fclean: clean 
		make fclean -C ./libft
		rm -rf $(NAME)

re: fclean all
