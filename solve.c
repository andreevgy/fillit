/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 19:25:25 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:27:18 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
**	check_place
**	Получает карту, размер карты, деталь и координаты левой верхней ячейки
**	Проверяет, может ли поставить туда деталь
*/

static int	check_place(char **map, int size, t_tetr *list, t_coords *coords)
{
	int	x;
	int	y;

	if (coords->x + list->width > size || coords->y + list->height > size)
		return (0);
	y = 0;
	while (y < list->height)
	{
		x = 0;
		while (x < list->width)
		{
			if (map[coords->y + y][coords->x + x] != '.'
				&& list->figure[y][x] != '.')
				return (0);
			x++;
		}
		y++;
	}
	return (1);
}

/*
**	set_figure
**	Ставит фигуру по заданным координатам на карту
*/

static void	set_figure(char **map, t_tetr *list, t_coords *coords)
{
	int	x;
	int	y;

	y = 0;
	while (y < list->height)
	{
		x = 0;
		while (x < list->width)
		{
			if (list->figure[y][x] != '.')
				map[coords->y + y][coords->x + x] = list->symbol;
			x++;
		}
		y++;
	}
}

/*
**	solve_map
**	Получает карту, размер карты, деталь, ставит деталь на первое подходящее
**	место, после чего вызывает себя рекурсивно со следующей деталью.
**	Если рекурсия вернула 0, двигаем деталь и повторяем
**	Возвращаем решенную карту
*/

int			solve_map(char ***map, int size, t_tetr *list)
{
	t_coords	*coords;
	char		**new_map;

	new_map = NULL;
	copy_map(map, &new_map, size);
	coords = create_coords(-1, -1);
	while (++coords->y < size)
	{
		coords->x = -1;
		while (++coords->x < size)
		{
			if (check_place(*map, size, list, coords))
			{
				set_figure(new_map, list, coords);
				if (list->next == NULL)
					return (copy_and_clean(&new_map, size, map, coords));
				else if (solve_map(&new_map, size, list->next))
					return (copy_and_clean(&new_map, size, map, coords));
				else
					copy_map(map, &new_map, size);
			}
		}
	}
	return (delete_last(new_map, list, coords, size));
}
