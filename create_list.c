/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 16:27:27 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:26:11 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		delete_coords(t_coords *c1, t_coords *c2, t_coords *c3)
{
	free(c1);
	free(c2);
	free(c3);
}

/*
**	find_coords
**	Принимает необрезанную фигуру и указатели на структуры координат верхней
**	левой и нижней правой точки
**	Устанавливает нужные для обрезки значения координат
*/

static void		find_coords(char **figure, t_coords **left_up,
							t_coords **right_down, char symbol)
{
	int			x;
	int			y;

	y = -1;
	while (++y < 4)
	{
		x = -1;
		while (++x < 4)
		{
			if (figure[y][x] == '#')
			{
				figure[y][x] = symbol;
				if ((*left_up)->x > x)
					(*left_up)->x = x;
				if ((*left_up)->y > y)
					(*left_up)->y = y;
				if ((*right_down)->x < x)
					(*right_down)->x = x;
				if ((*right_down)->y < y)
					(*right_down)->y = y;
			}
		}
	}
}

/*
**	cut_empty
**	Принимает необрезанную фигуру
**	Обрезает пустые точки по сторонам фигуры и возвращает ее
*/

static char		**cut_empty(char **figure, int *height, int *width, char symbol)
{
	t_coords	*l_u;
	t_coords	*r_d;
	t_coords	*iter;
	char		**new_figure;
	int			i;

	l_u = create_coords(3, 3);
	r_d = create_coords(0, 0);
	find_coords(figure, &l_u, &r_d, symbol);
	iter = create_coords(l_u->x, l_u->y);
	NULL_CHECK((new_figure = malloc(sizeof(char *) * (r_d->y - l_u->y + 2))));
	i = 0;
	while (iter->y <= r_d->y)
	{
		iter->x = l_u->x;
		new_figure[i] = ft_strsub(figure[iter->y], l_u->x, r_d->x - l_u->x + 1);
		i++;
		iter->y++;
	}
	new_figure[i] = NULL;
	*height = r_d->y - l_u->y + 1;
	*width = r_d->x - l_u->x + 1;
	delete_coords(l_u, r_d, iter);
	return (new_figure);
}

/*
**	create_list
**	Принимает список и необрезанную фигуру
**	Обрезает фигуру, создает структуру и ставит ее в конец
*/

void			create_list(t_tetr **list, char **figure)
{
	t_tetr	*new_list;
	t_tetr	*iter;

	NULL_CHECK((new_list = malloc(sizeof(t_tetr))));
	new_list->next = NULL;
	new_list->symbol = 'A';
	if (!*list)
	{
		new_list->figure = cut_empty(figure, &(new_list->height),
									&(new_list->width), new_list->symbol);
		*list = new_list;
	}
	else
	{
		iter = *list;
		while (iter->next != NULL)
			iter = iter->next;
		new_list->symbol = iter->symbol + 1;
		new_list->figure = cut_empty(figure, &(new_list->height),
									&(new_list->width), new_list->symbol);
		iter->next = new_list;
	}
}
