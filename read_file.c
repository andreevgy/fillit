/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 12:06:21 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:18:53 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./fillit.h"
#include <stdio.h>

void		clean_lines(char **lines)
{
	int k;

	k = 0;
	while (lines[k])
	{
		ft_strdel(&lines[k]);
		k++;
	}
	free(lines);
}

static void	*split_and_validate(char ***lines, t_tetr **list, char *string)
{
	NULL_CHECK((*lines = ft_strsplit(string, '\n')));
	if (!ft_figure_validation(*lines))
		return (NULL);
	create_list(list, *lines);
	clean_lines(*lines);
	return ((void *)1);
}

/*
**	Читаем файл по 21 символу ((4 символа + \n) & * 4 + последний \n)
**	Валидируем каждую ячейку
**	Заносим в список
**	Остается 19 символов (3 строки по 5 символов и последние 4 символа)
**	Валидируем их и заносим в список
**	Возвращаем список
*/

t_tetr		*read_file(char *file_name)
{
	int		fd;
	char	string[22];
	char	**lines;
	int		ret;
	t_tetr	*list;

	list = NULL;
	if ((fd = open(file_name, O_RDONLY)) == -1)
		return (NULL);
	while ((ret = read(fd, string, 21)) == 21)
	{
		string[ret] = '\0';
		if (!split_and_validate(&lines, &list, string))
			return (NULL);
	}
	if (ret != 20)
		return (NULL);
	string[ret] = '\0';
	if (!split_and_validate(&lines, &list, string))
		return (NULL);
	if (read(fd, string, 21))
		return (NULL);
	close(fd);
	return (list);
}
