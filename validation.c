/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmacgyve <fmacgyve@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 12:43:17 by fkhrazz           #+#    #+#             */
/*   Updated: 2018/12/25 15:08:21 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static	int		ft_pre_validation(char *line)
{
	int i;

	i = 0;
	while (line[i])
	{
		if (line[i] != '.' && line[i] != '#')
			return (0);
		i++;
	}
	if (i != 4)
		return (0);
	return (1);
}

static int		ft_check_around(char **lines, int x, int y)
{
	int sum;

	sum = 0;
	if (lines[x] && lines[x][y - 1])
	{
		if (lines[x][y - 1] == '#')
			sum++;
	}
	if (lines[x] && lines[x][y + 1])
	{
		if (lines[x][y + 1] == '#')
			sum++;
	}
	if (x >= 1 && lines[x - 1] && lines[x - 1][y])
	{
		if (lines[x - 1][y] == '#')
			sum++;
	}
	if (lines[x + 1] && lines[x + 1][y])
	{
		if (lines[x + 1][y] == '#')
			sum++;
	}
	return (sum);
}

int				ft_figure_validation(char **lines)
{
	int i;
	int j;
	int sum;
	int hash;

	i = -1;
	sum = 0;
	hash = 0;
	while (++i < 4)
	{
		j = -1;
		if (!ft_pre_validation(lines[i]))
			return (0);
		while (++j < 4)
		{
			if (lines[i][j] == '#')
			{
				sum += ft_check_around(lines, i, j);
				hash++;
			}
		}
	}
	if (sum < 6 || hash != 4)
		return (0);
	return (1);
}
