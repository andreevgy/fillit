/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkhrazz <fkhrazz@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/23 16:20:18 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 16:26:30 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		copy_and_clean(char ***new_map, int size, char ***map, t_coords *coords)
{
	clean_map(*map, size);
	copy_map(new_map, map, size);
	clean_map(*new_map, size);
	free(coords);
	return (1);
}

int		delete_last(char **new_map, t_tetr *list, t_coords *coords, int size)
{
	if (new_map && list->next == NULL)
		clean_map(new_map, size);
	free(coords);
	return (0);
}

void	clean_map(char **map, int size)
{
	int	y;

	y = 0;
	while (map[y] && y < size)
		free(map[y++]);
	free(map);
}
