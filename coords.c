/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coords.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmacgyve <fmacgyve@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 16:36:53 by fmacgyve          #+#    #+#             */
/*   Updated: 2018/12/25 15:08:09 by fkhrazz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_coords	*create_coords(int x, int y)
{
	t_coords	*coords;

	NULL_CHECK((coords = malloc(sizeof(coords))));
	coords->x = x;
	coords->y = y;
	return (coords);
}
